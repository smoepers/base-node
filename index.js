// Trigger base-node command with requested options/flags
exports.call = () => {
  const arg_reader = require('command-line-args');

  const supported_args = [
    { name : 'verbose', alias : 'v', type : Boolean },
    { name : 'project_name', alias : 'n', type : String, defaultOption : true },
    { name : 'project_repo_host', alias : 'r', type : String }
  ];

  const args = arg_reader(supported_args);

  if (args.project_name) {
    const { createByName } = require('./create');

    createByName(args.project_name, args)
      .then(res => {
        console.log('Successfully created', res);
      })
      .catch(e => {
        console.log('Failed to create', e);
      });
  }
};
