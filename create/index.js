// Create a new project next as base-node's sibling (in base-node's root)
const ncp = require('ncp').ncp;
const fs = require('fs');
const replace = require('stream-replace');
const replacements = require('./replacements');

ncp.limit = 16;

exports.createByName = (project_name, args) => {
  console.log(args);
  const project_replacements = replacements.get(args);
  const destination = `../${project_name}`;

  const ncp_options = {
    transform : (read, write) => {
      Object.keys(project_replacements).forEach(tag => {
        read = read.pipe(
          replace(
            new RegExp(`{{${tag}}}`, 'g'),
            project_replacements[tag] || `|${tag}|`
          )
        );
      });

      read.pipe(write);
    }
  };

  return new Promise((resolve, reject) => {
    if (fs.existsSync(destination)) {
      reject({ err : { reason : 'Project root already exists' } });
    }

    ncp('./create/files', destination, ncp_options, function (err) {
      if (err) {
        return reject({ err : err });
      }

      resolve({ created : project_name });
    });
  });
};
