const defaults = {
  project_name : 'no-name',
  project_repo_host : 'https://bitbucket.org/smoepers/'
};

exports.get = args => {
  return Object.assign({}, defaults, args);
};
